import { 
  Controller,
  Get,
  Query,
  Body,
  Req,
  UseGuards,
  Param
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Roles } from '../commons/roles.decorator'
import { AuthGuard } from '../auth.guard'
import { RetrieveService } from './retrieve.service';
import { RetrieveInput } from './retrieve.dto'
import { KFoldService } from './kfold.service'
import { Record } from '../db/record.entity'
import { RecordType } from '../db/record.entity'

@Controller('v1/kfold')
export class KFoldController {

  constructor (
    private readonly _kfold: KFoldService,

    @InjectRepository(Record)
    private readonly _record: Repository<Record>
  ) {}

  @Get(':k')
  public async kfold (@Param('k') k: string, @Query('indexing') indexing: string) {
    const _k = parseInt(k)
    const _indexing = indexing ? true : false
    console.log('indexing=', _indexing)
    let data = await this._record.find({ where: { tipe: RecordType.CASE }, relations: ['listGejala', 'hamaPenyakit'] })
    // data = shuffle(data)
    const result = this._kfold.run(data, _k, _indexing)
    return result
  }
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
