import { 
  Controller,
  Post,
  Body,
  Req,
  UseGuards
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Roles } from '../commons/roles.decorator'
import { AuthGuard } from '../auth.guard'
import { RetrieveService } from './retrieve.service';
import { RetrieveInput } from './retrieve.dto'
import { KFoldService } from './kfold.service'
import { Record } from '../db/record.entity'

@Controller('v1/retrieve')
export class RetrieveController {

  constructor (
    private readonly retrieveService: RetrieveService,
    private readonly kfoldService: KFoldService,

    @InjectRepository(Record)
    private readonly _record: Repository<Record>
  ) {}

  @Post()
  @Roles('admin', 'user')
  @UseGuards(AuthGuard)
  public async retrieve(
    @Req() req: any,
    @Body() payload: RetrieveInput
  ) {
    const userId = req.user.id
    return await this.retrieveService.retrieve(userId, payload)
  }
}
