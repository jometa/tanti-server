import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import { RetrieveService } from './retrieve.service';
import { RetrieveController } from './retrieve.controller';
import { KFoldService } from './kfold.service'
import { KFoldController } from './kfold.controller'
import { Record } from '../db/record.entity'

@Module({
  imports: [
    TypeOrmModule.forFeature([ Record ])
  ],
  providers: [RetrieveService, KFoldService],
  controllers: [RetrieveController, KFoldController]
})
export class RetrieveModule {}
