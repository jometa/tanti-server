import { Injectable } from '@nestjs/common'
import { Record, RecordType } from '../db/record.entity'
import { NaiveBayes, NBRow } from './NaiveBayes'

type TestResult = {
  row: NBRow
  real: number
  system: number
  similarity: number
}

type PartResult = {
  accuracy: number
  totalHit: number
  results: TestResult[]
}

type KFoldResult = {
  partitions: PartResult[]
  accuracy: number
}

function toNbRow (r: Record) : NBRow {
  if (!r.listGejala) throw new Error(`r = ${JSON.stringify(r)}`)
  return {
    attributes: r.listGejala.map(({ id }) => id),
    target: r.idHamaPenyakit,
    id: r.id
  }
}

type Runner = (x: NBRow, nb: NaiveBayes) => TestResult

function testSingleData_Indexing (x: NBRow, nb: NaiveBayes) : TestResult {
  const real = x.target
  const [ system_target, system_prob ] = nb.classify(x.attributes)
  const result = nb.knn(x.attributes, system_target)
  return {
    row: x,
    real,
    system: system_target,
    similarity: result.similarity
  }
}

function testSingleData_NonIndexing (x: NBRow, nb: NaiveBayes) : TestResult {
  const real = x.target
  const result = nb.knn_only(x.attributes)
  return {
    row: x,
    real,
    system: result.row.target,
    similarity: result.similarity
  }
}

@Injectable()
export class KFoldService {

  private _runTest(trainData: Record[], testData: Record[], indexing: boolean) : PartResult {
    const _trainData: NBRow[] = trainData.map(toNbRow)
    const _testData: NBRow[] = testData.map(toNbRow)

    const nb = new NaiveBayes(_trainData)
    nb.build()
    const runner: Runner = indexing ? testSingleData_Indexing : testSingleData_NonIndexing
    const testResults = _testData.map(x => runner(x, nb))
    
    // Compute total hit
    const totalHit = testResults.map(({ real, system }) => real == system ? 1 : 0).reduce((p, n) => p + n, 0.0)
    const accuracy = totalHit / _testData.length

    return {
      results: testResults,
      accuracy,
      totalHit
    }
  }

  private splitDataset (data: Record[], k: number) : Record[][] {
    let part_nums = [ ...Array(k).keys() ]
    let partitions: Record[][] = part_nums.map(i => [])
    data.forEach((r, idx) => {
      const part_idx = idx % k
      partitions[part_idx].push(r)
    })

    return partitions
  }

  private runTest (partitions: Record[][], indexing: boolean) : KFoldResult {
    const k = partitions.length
    let part_nums = [ ...Array(k).keys() ]
    const result = part_nums.map(part_idx => {
      const train_data = partitions.filter((part, idx) => idx !== part_idx).reduce((p, n) => p.concat(n), [])
      const test_data = partitions[part_idx]

      return this._runTest(train_data, test_data, indexing)
    })
    
    // Calculate total accuracy
    const total_acc = result.map(p => p.accuracy).reduce((p, c) => p + c, 0)
    const acc = 1.0 * total_acc / k
    return {
      partitions: result,
      accuracy: acc
    }
  }

  public run (data: Record[], k: number, indexing: boolean) : KFoldResult {
    const partitions = this.splitDataset(data, k)
    const result = this.runTest(partitions, indexing)
    return result
  }
}