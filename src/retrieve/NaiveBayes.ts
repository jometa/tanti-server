type Target = number
type Attribute = number
type NBClassAttrSummary = Map<Attribute, number>
type NBAttributeSummary = Map<Target, NBClassAttrSummary>
type NBInput = number[]
export type NBRow = {
  attributes: Attribute[]
  target: Target
  id: number
}

export class NaiveBayes {

  allAtributes: Set<number> = new Set<number>()
  clazz_summary: Map<number, number> = undefined
  attributes_summary: Map<Target, NBClassAttrSummary> = undefined
  
  constructor (
    private rows: NBRow[]
  ) {
    rows.forEach(row => {
      row.attributes.forEach(attr => {
        this.allAtributes.add(attr)
      })
    })
  }

  summarizeClass () : Map<number, number> {
    let result = new Map<number, number>()
    this.rows.forEach(row => {
      if (!result.has(row.target)) {
        result.set(row.target, 0)
      }
      const current = result.get(row.target)
      result.set(row.target, current + 1)
    })
    return result
  }

  summarizeAttributeByClass (_clazz: Target) : NBClassAttrSummary {
    const _clazzRows = this.rows.filter(it => it.target === _clazz)
    let result = new Map<number, number>()
    this.allAtributes.forEach(attr => {
      result.set(attr, 0)
    })

    _clazzRows.forEach(row => {
      row.attributes.forEach(attr => {
        const current = result.get(attr)
        result.set(attr, current + 1)
      })
    })

    return result
  }

  build () {
    this.clazz_summary = this.summarizeClass()
    this.attributes_summary = new Map<Target, NBClassAttrSummary>()
    const allClazz = Array.from(this.clazz_summary.keys())
    for (let clazz of allClazz) {
      const class_atr_summary: NBClassAttrSummary = this.summarizeAttributeByClass(clazz)
      this.attributes_summary.set(clazz, class_atr_summary)
    }
  }

  classify (input: NBInput) {
    const allClazz = Array.from(this.clazz_summary.keys())
    const total_case = this.rows.length
    let result = []
    for (let clazz of allClazz) {
      const count_clazz = this.clazz_summary.get(clazz)
      const prob_clazz = count_clazz * 1.0 / total_case
      const clz_attribute_summary = this.attributes_summary.get(clazz)
      const attr_probs = input.map((attr, index) => {
        return (clz_attribute_summary.get(attr) * 1.0) / (count_clazz);
      })
      let total_prod = 1
      total_prod = attr_probs.reduce((a, b) => a * b, 1)
      total_prod *= prob_clazz
      result.push([clazz, total_prod])
    }

    
    result.sort((a, b) => b[1] - a[1])
    
    // result.reverse()
    // console.log('===result (reverse)===');
    // console.log(result);
    // console.log();
    return result[0]
  }

  knn (attributes: Attribute[], clazz: number) {
    const indexedDataset = this.rows.filter(({ target }) => target === clazz)

    const a = new Set(attributes)
    const knnResult = indexedDataset.map(row => {
      const b = new Set(row.attributes)
      const similarity = sim(a, b)
      return { row, similarity }
    })

    // console.log('knnResult=', knnResult)
    knnResult.sort((a, b) => b.similarity - a.similarity)
    const maxKnnResult = knnResult[0]
    // console.log('maxKnn=', maxKnnResult)
    return maxKnnResult
  }

  knn_only (attributes: Attribute[]) {
    const indexedDataset = this.rows
    const a = new Set(attributes)
    const knnResult = indexedDataset.map(row => {
      const b = new Set(row.attributes)
      const similarity = sim(a, b)
      return { row, similarity }
    })

    // console.log('knnResult=', knnResult)
    knnResult.sort((a, b) => b.similarity - a.similarity)
    const maxKnnResult = knnResult[0]
    // console.log('maxKnn=', maxKnnResult)
    return maxKnnResult
  }

}

function sim(a:Set<number>, b:Set<Number>) : number {
  let hit = 0
  let totalData = (new Set([...a, ...b])).size

  let first = undefined
  let second = undefined
  if (a.size > b.size) {
    first = b
    second = a
  } else {
    first = a
    second = b
  }

  for (let el_x of first) {
    for (let el_y of second) {
      if (el_x == el_y) {
        hit += 1
        break
      }
    }
  }

  return hit * 1.0 / (totalData)
}