
## Naive Bayes

### 1. Fungsi untuk menghitung kemunculan tiap kelas dalam dataset
```typescript class:"lineNo"
summarizeClass () : Map<number, number> {
  let result = new Map<number, number>()
  this.rows.forEach(row => {
    if (!result.has(row.target)) {
      result.set(row.target, 0)
    }
    const current = result.get(row.target)
    result.set(row.target, current + 1)
  })
  return result
}
```

#### Penjelasan
```typescript class:"lineNo"
summarizeClass () : Map<number, number> {
```
Definisi Fungsi Untuk menghitung kemunculan tiap kelas dalam dataset


```typescript class:"lineNo"
  let result = new Map<number, number>()
```
Menginisialisasi variabel untuk menyimpan hasil hitungan


```typescript
  this.rows.forEach(row => {
```
Iterasi tiap data dalam dataset.


```typescript
  if (!result.has(row.target)) {
    result.set(row.target, 0)
  }
```
Jika kelas dari data belum pernah dihitung sebelumnya, Inisialisasi hitungan untuk kelas tersebut.


```typescript
  const current = result.get(row.target)
  result.set(row.target, current + 1)
```
Menaikan hitungan untuk tiap kelas yang ditemukan.


### 2. Fungsi untuk menghitung kemunculan tiap attribut dalam kelas tertentu.
```typescript
summarizeAttributeByClass (_clazz: Target) : NBClassAttrSummary {
  const _clazzRows = this.rows.filter(it => it.target === _clazz)
  let result = new Map<number, number>()
  this.allAtributes.forEach(attr => {
    result.set(attr, 0)
  })

  _clazzRows.forEach(row => {
    row.attributes.forEach(attr => {
      const current = result.get(attr)
      result.set(attr, current + 1)
    })
  })

  return result
}

```

#### Penjelasan

```typescript
summarizeAttributeByClass (_clazz: Target) : NBClassAttrSummary
```
Definisi fungsi.


```typescript
  const _clazzRows = this.rows.filter(it => it.target === _clazz)
```
Seleksi data yang memiliki kelas tertentu.


```typescript
  let result = new Map<number, number>()
```
Inisialisasi variabel penampung dengan tipe data ```HashMap``` untuk hitungan kemunculan attribut.


```typescript
this.allAtributes.forEach(attr => {
  result.set(attr, 0)
})
```
Inisialisasi counter untuk tiap attribut dengan 0.


```typescript
_clazzRows.forEach(row => 
```
Iterasi tiap data dalam dataset.


```typescript
row.attributes.forEach(attr =>
```
Iterasi tiap attribut dalam data.


```typescript
const current = result.get(attr)
```
Mengambil hitungan sementara dari attribut.

```typescript
result.set(attr, current + 1)
```
Meningkatkan hitungan dari attribut.


### 3. Fungsi Klasifikasi
```typescript
classify (input: NBInput) {
  const allClazz = Array.from(this.clazz_summary.keys())
  const total_case = this.rows.length
  let result = []
  for (let clazz of allClazz) {
    const count_clazz = this.clazz_summary.get(clazz)
    const prob_clazz = count_clazz * 1.0 / total_case
    const clz_attribute_summary = this.attributes_summary.get(clazz)
    const attr_probs = input.map((attr, index) => {
      return clz_attribute_summary.get(attr) * 1.0 / count_clazz
    })
    let total_prod = 1
    total_prod = attr_probs.reduce((a, b) => a * b, 1)
    total_prod *= prob_clazz
    result.push([clazz, total_prod])
  }
  
  result.sort((a, b) => b[1] - a[1])
  return result[0]
}
```

#### Penjelasan

```typescript
classify (input: NBInput)
```
Definisi fungsi klasifikasi.

```typescript
const allClazz = Array.from(this.clazz_summary.keys())
```
Menginisialisasi array yang berisi semua kelas.

```typescript
const total_case = this.rows.length
```
Menghitung jumlah dataset.

```typescript
let result = []
```
Menginisialisasi array hasil.


```typescript
for (let clazz of allClazz)
```
Iterasi tiap kelas.


```typescript
const count_clazz = this.clazz_summary.get(clazz)
```
Hitung jumlah data dalam kelas.


```typescript
const prob_clazz = count_clazz * 1.0 / total_case
```
Menghitung probabilitas kelas.

```typescript
const attr_probs = input.map((attr, index) => {
  return clz_attribute_summary.get(attr) * 1.0 / count_clazz
})
```
menghitung probabilitas tiap attribut dalam kelas.

```typescript
let total_prod = 1
total_prod = attr_probs.reduce((a, b) => a * b, 1)
total_prod *= prob_clazz
```
Mengalikan probabilitas kelas dan attribut

```typescript
result.sort((a, b) => b[1] - a[1])
return result[0]
```
Mengambil nilai attribut tertinggi.