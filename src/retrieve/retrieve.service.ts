import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm'
import { Connection } from 'typeorm'
import { Gejala } from '../db/gejala.entity'
import { HamaPenyakit } from '../db/hamapenyakit.entity'
import { Record, RecordType } from '../db/record.entity'
import * as moment from 'moment'
import axios from 'axios'
import { RetrieveInput } from './retrieve.dto';
import { NaiveBayes } from './NaiveBayes'

// type GejalaCounter = Map<number, number>

const LAMBDA_URL = process.env.LAMBDA_URL || 'http://localhost:5001/api/tanti-bianome'

class GejalaCounter {
  private data: Map<number, number> = new Map<number, number>()

  public increment (idGejala: number) {
    if (!this.data.has(idGejala)) {
      this.data.set(idGejala, 0)
    }
    const currentCount = this.data.get(idGejala)
    this.data.set(idGejala, currentCount + 1)
  }

  public count(idGejala) {
    if (!this.data.has(idGejala)) {
      return 0
    }
    return this.data.get(idGejala)
  }
}

class HpCount {
  public submap: GejalaCounter = new GejalaCounter()
  public count: number = 0

  public increment () {
    this.count += 1
  }  
}

type HpCounter = Map<number, HpCount>

@Injectable()
export class RetrieveService {

  constructor (
    @InjectConnection()
    private readonly connection: Connection
  ) {}

  public async retrieve (userId: number, payload: RetrieveInput) {
    const hpRepo = this.connection.getRepository<HamaPenyakit>(HamaPenyakit)
    const gejalaRepo = this.connection.getRepository<Gejala>(Gejala)
    const recordRepo = this.connection.getRepository<Record>(Record)

    const listGejala = await gejalaRepo.findByIds(payload.listGejala)

    let record = recordRepo.create({
      idAddedBy: userId,
      listGejala,
      desa: payload.desa,
      umurPadi: payload.umurPadi,
      idHamaPenyakit: 4,
      similarity: 0.8,
      tipe: RecordType.RECORD,
      waktu: moment().toDate()
    })

    const allRecords = await recordRepo.find({
      where: {
        tipe: RecordType.CASE
      },
      relations: ['listGejala', 'hamaPenyakit']
    })
    const nbRows = allRecords.map(record => ({
      attributes: record.listGejala.map(g => g.id),
      target: record.idHamaPenyakit,
      id: record.id
    }))
    const nb = new NaiveBayes(nbRows)
    nb.build()
    const [ clazz, clazz_prob ] = nb.classify(payload.listGejala)
    const result = nb.knn(payload.listGejala, clazz)
    console.log('result=', result)

    const mostSimCase = await recordRepo.findOne(result.row.id)
    record.similarity = result.similarity
    record.idHamaPenyakit = result.row.target
    record.solusi = mostSimCase.solusi
    record = await recordRepo.save(record)
    return record
  }

  private async indexing () {
    const hpRepo = this.connection.getRepository<HamaPenyakit>(HamaPenyakit)
    const gejalaRepo = this.connection.getRepository<Gejala>(Gejala)
    const recordRepo = this.connection.getRepository<Record>(Record)

    const allHp = await hpRepo.find()
    const allGejala = await gejalaRepo.find()
    const allKasus = await recordRepo.find({
      tipe: RecordType.CASE
    })

    let hpCount : HpCounter = new Map<number, HpCount>()

    allKasus.forEach(kasus => {
      const { hamaPenyakit, listGejala } = kasus
      const idHp = hamaPenyakit.id

      if (!hpCount.has(idHp)) {
        hpCount.set(idHp, new HpCount())
      }
      const currentHpCount = hpCount.get(idHp)
      currentHpCount.increment()

      const gejalaCounter = currentHpCount.submap
      listGejala.forEach(gejala => {
        const idGejala = gejala.id
        gejalaCounter.increment(idGejala)
      })
    })

    const nKasus = allKasus.length
  }

}
