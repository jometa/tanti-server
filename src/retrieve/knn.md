## KNN
```typescript class:"lineNo"
knn (attributes: Attribute[], clazz: number) {
  const indexedDataset = this.rows.filter(({ target }) => target === clazz)
  const a = new Set(attributes)
  const knnResult = indexedDataset.map(row => {
    const b = new Set(row.attributes)
    const similarity = sim(a, b)
    return { row, similarity }
  })

  // console.log('knnResult=', knnResult)
  knnResult.sort((a, b) => b.similarity - a.similarity)
  const maxKnnResult = knnResult[0]
  // console.log('maxKnn=', maxKnnResult)
  return maxKnnResult
}
```

#### Penjelasan
```typescript class:"lineNo"
knn (attributes: Attribute[], clazz: number)
```
Definisi fungsi knn.

```typescript"
const indexedDataset = this.rows.filter(({ target }) => target === clazz)
```
Ambil data hasil indexing.

```typescript"
const a = new Set(attributes)
```
Konversi attribute kasus baru kedalam vektor.

```typescript"
const a = new Set(attributes)
```
Konversi attribute kasus baru kedalam vektor.

```typescript"
indexedDataset.map(row => {...})
```
Iterasi tiap kasus lama dalam data hasil indexing.

```typescript"
const b = new Set(row.attributes)
```
Konversi data kasus lama kedalam vektor.

```typescript"
const similarity = sim(a, b)
```
Hitung similaritas.


```typescript
knnResult.sort((a, b) => b.similarity - a.similarity)
```
Urutkan berdasarkan similaritas tertinggi.