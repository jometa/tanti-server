import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class HamaPenyakit {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nama: string

  @Column('text')
  keterangan: string;

  @Column({ length: 10, nullable: true })
  appId: string;

  @Column()
  deleted: boolean;
}