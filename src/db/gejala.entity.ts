import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Gejala {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nama: string;

  @Column('text', { nullable: true })
  keterangan: string;

  @Column({ length: 10, nullable: true })
  appId: string;

  @Column({ default: '0' })
  deleted: boolean;
}