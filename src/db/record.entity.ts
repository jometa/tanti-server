import { 
  Entity, 
  Column, 
  PrimaryGeneratedColumn,
  ManyToMany,
  ManyToOne,
  JoinColumn,
  JoinTable
} from 'typeorm'
import { User } from './user.entity'
import { Gejala } from './gejala.entity'
import { HamaPenyakit } from './hamapenyakit.entity'

export enum RecordType {
  CASE='CASE',
  REVISE='REVISE',
  RECORD='RECORD'
}

@Entity()
export class Record {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  waktu: Date;

  @Column({ nullable: true })
  desa: string;

  @Column({ nullable: true })
  umurPadi: string;

  @Column('json', { nullable: true })
  solusi: string;

  @Column('float', { default: '0.0' })
  similarity: number;

  @Column()
  tipe: RecordType;

  @ManyToMany(type => Gejala)
  @JoinTable()
  listGejala: Gejala[]

  @Column('int', { nullable: true })
  idHamaPenyakit: number;

  @ManyToOne(type => HamaPenyakit)
  @JoinColumn({ name: 'idHamaPenyakit' })
  hamaPenyakit: HamaPenyakit;

  @Column('int')
  idAddedBy: number;

  @ManyToOne(type => User)
  @JoinColumn({ name: 'idAddedBy' })
  addedBy: User;
}