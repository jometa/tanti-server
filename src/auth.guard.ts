import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm'
import { Connection } from 'typeorm'
import * as jwt from 'jsonwebtoken'
import { Reflector } from '@nestjs/core';
import { User } from './db/user.entity'
import { UserRequest as UserRequestObject } from './commons/user-request.dto'

const APP_SECRET = process.env.APP_SECRET

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private readonly reflector: Reflector
  ) {}
  
  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {
    const request = context.switchToHttp().getRequest()
    let user = request.user

    if (!user) return false

    // Retrieve the roles passed to (Roles) decorator
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }

    return roles.some(role => user.role == role)
  }
}