// Dotenv configuration
import * as dotenv from 'dotenv'
dotenv.config()

import { join } from 'path'
import { NestFactory, HttpAdapterHost } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as session from 'express-session'
import { AppModule } from './app.module';

// Express specific imports
const moment = require('moment');

// Additional functions passed to pug.js
import { parseSolusi } from './commons/parseSolusi';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  
  app.enableCors();
  app.useStaticAssets(join(__dirname, '..', 'public'))
  app.setBaseViewsDir(join(__dirname, '..', 'views'))
  app.setViewEngine('pug')
  app.use(session({
    secret: process.env.APP_SECRET,
    secure: true,
    saveUnitialized: true
  }))
  
  const options = new DocumentBuilder()
  .setTitle('CBR with KNN and Naive Bayes')
    .setDescription('API description')
    .setVersion('1.0')
    .addTag('cbr')
    .build();
  const document = SwaggerModule.createDocument(app, options);
    
  SwaggerModule.setup('api', app, document);

  // Get express instance.
  const adapterHost = app.get(HttpAdapterHost);
  const httpAdapter = adapterHost.httpAdapter;
  const expressInstance = httpAdapter.getInstance();

  // Register moment.js to view
  expressInstance.locals.moment = moment;
  expressInstance.locals.parseSolusi = parseSolusi;

  await app.listen(process.env.PORT);
}

bootstrap();
