import { Injectable, NestMiddleware, Inject } from '@nestjs/common'
import { Request, Response } from 'express'
import { UserService } from './user/user.service';
import { UserRequest as UserRequestObject  } from './commons/user-request.dto'

declare module 'express' {
  interface Request {
    user?: UserRequestObject
  }
}

@Injectable() 
export class WithUser implements NestMiddleware {

  constructor (
    private readonly userService: UserService
  ) {}

  async use (req: any, resp: Response, next: Function) {
    const authHeader = req.headers.authorization
    // console.log('authHeader=', authHeader)
    // console.log('authHeader=', req.session)
    const token = authHeader ? authHeader.split(' ')[1] : req.session.token
    // console.log('token=', token)
    // Token must be greater than 40
    if (token == null || token == undefined || token.length < 40) {
      console.log('must be here')
      next()
      return
    }
    const userObject = await this.userService.decodeToken(token)
    req.user = userObject
    next()
  }
}