import { ApiModelProperty } from "@nestjs/swagger";

export class CreateHpInput {
  @ApiModelProperty({
    description: 'Name of (HamaPenyakit)'
  })
  nama: string

  @ApiModelProperty({
    description: 'Description (HamaPenyakit)'
  })
  keterangan: string

  @ApiModelProperty({
    description: 'Additional Application ID'
  })
  appId: string
}