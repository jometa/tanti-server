import { ApiModelProperty } from "@nestjs/swagger";

export class CreateGejalaInput {
  @ApiModelProperty({
    description: 'Name of gejala'
  })
  nama: string

  @ApiModelProperty({
    description: 'Description of this gejala'
  })
  keterangan: string

  @ApiModelProperty({
    description: 'Additional Application Id'
  })
  appId: string
}