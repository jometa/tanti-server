import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'
import { Like, Repository } from 'typeorm'
import { Gejala } from '../db/gejala.entity'
import { HamaPenyakit } from '../db/hamapenyakit.entity'
import { CreateGejalaInput } from './CreateGejalaInput.dto'
import { CreateHpInput } from './CreateHpInput.dto'
import { SearchInput } from '../commons/SearchInput.dto'

@Injectable()
export class TagService {

  constructor (
    @InjectRepository(Gejala)
    private readonly gejalaRepository: Repository<Gejala>,

    @InjectRepository(HamaPenyakit)
    private readonly hpRepository: Repository<HamaPenyakit>
  ) {}

  public async createGejala (payload: CreateGejalaInput) : Promise<Gejala> {
    let gejala = this.gejalaRepository.create({
      ...payload
    })
    gejala = await this.gejalaRepository.save(gejala)
    return gejala
  }

  public async findGejalaById (id: number) : Promise<Gejala> {
    return await this.gejalaRepository.findOneOrFail(id, {
      where: {
        deleted: false
      }
    })
  }

  public async findGejala (input: SearchInput) : Promise<Gejala[]> {
    const keyword = input.keyword ? input.keyword : ''
    return await this.gejalaRepository.find({
      where: {
        nama: Like(`%${keyword}%`),

        // Don't return deleted gejala
        deleted: false
      },
      skip: input.skip,
      take: input.take,
      order: {
        nama: 'ASC'
      }
    })
  }

  public async removeGejala (id: number) : Promise<boolean> {
    let gejala = await this.gejalaRepository.findOneOrFail(id)
    gejala.deleted = true
    await this.gejalaRepository.save(gejala)
    return true
  }

  public async updateGejala (id: number, payload: CreateGejalaInput) : Promise<Gejala> {
    let gejala = await this.gejalaRepository.findOneOrFail(id)
    gejala.nama = payload.nama
    gejala.keterangan = payload.keterangan
    gejala.appId = payload.appId
    await this.gejalaRepository.save(gejala)
    return gejala
  }

  public async createHp (payload: CreateHpInput) : Promise<HamaPenyakit> {
    let hamaPenyakit = this.hpRepository.create({
      ...payload,
      deleted: false
    })
    hamaPenyakit = await this.hpRepository.save(hamaPenyakit)
    return hamaPenyakit
  }

  public async findHpById (id: number) : Promise<HamaPenyakit> {
    return await this.hpRepository.findOneOrFail(id, {
      where: {
        // Don't return deleted data
        deleted: false
      }
    })
  }

  public async findHp (input: SearchInput) : Promise<HamaPenyakit[]> {
    const keyword = input.keyword ? input.keyword : ''
    return await this.hpRepository.find({
      where: {
        keyword: Like(`%${keyword}%`),
        // Don't return deleted data
        deleted: false
      },
      skip: input.skip,
      take: input.take
    })
  }

  public async removeHp (id: number) {
    let hamaPenyakit = await this.hpRepository.findOneOrFail(id)
    hamaPenyakit.deleted = true
    await this.hpRepository.save(hamaPenyakit)
  }

  public async updateHp (id: number, payload: CreateHpInput) : Promise<HamaPenyakit> {
    let hamaPenyakit = await this.hpRepository.findOneOrFail(id)
    hamaPenyakit.nama = payload.nama
    hamaPenyakit.keterangan = payload.keterangan
    hamaPenyakit.appId = payload.appId
    await this.hpRepository.save(hamaPenyakit)
    return hamaPenyakit
  }

}
