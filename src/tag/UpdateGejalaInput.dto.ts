import { ApiModelProperty } from "@nestjs/swagger";

export class UpdateGejalaInput {
  @ApiModelProperty({
    description: 'ID of gejala'
  })
  id: number

  @ApiModelProperty({
    description: 'Name of gejala'
  })
  nama: string

  @ApiModelProperty({
    description: 'Description of gejala'
  })
  keterangan: string

  @ApiModelProperty({
    description: 'Additional Application ID'
  })
  appId: string
}