import { 
  Controller, 
  Get, Post, Delete, Put, 
  Body, Param, Query 
} from '@nestjs/common';
import { TagService } from './tag.service'
import { CreateGejalaInput } from './CreateGejalaInput.dto'
import { CreateHpInput } from './CreateHpInput.dto';
import { SearchInput } from '../commons/SearchInput.dto'

@Controller('v1/tag')
export class TagController {

  constructor (
    private readonly tagSevice: TagService
  ) {}

  @Post("gejala")
  public async createGejala (@Body() payload: CreateGejalaInput) {
    return await this.tagSevice.createGejala(payload)
  }

  @Get("gejala")
  public async findGejala (@Query() options: SearchInput) {
    return this.tagSevice.findGejala(options)
  }

  @Get("gejala/:id")
  public async findGejalaById (@Param("id") id: number) {
    return await this.tagSevice.findGejalaById(id)
  }

  @Delete("gejala/:id")
  public async removeGejalaById (@Param("id") id: number) {
    return await this.tagSevice.removeGejala(id)
  }

  @Put("gejala/:id")
  public async updateGejala (@Param('id') id: number, @Body() payload: CreateGejalaInput) {
    await this.tagSevice.updateGejala(id, payload)
    return 'OK'
  }

  @Post("hp")
  public async createHp (@Body() payload: CreateHpInput) {
    return await this.tagSevice.createHp(payload)
  }

  @Get("hp")
  public async findHp (@Query() options: SearchInput) {
    return await this.tagSevice.findHp(options)
  }

  @Get("hp/:id")
  public async findHpById (@Param("id") id: number) {
    return await this.tagSevice.findHpById(id)
  }

  @Delete("hp/:id")
  public async removeHpById (@Param("id") id: number) {
    await this.tagSevice.removeHp(id)
    return 'OK'
  }

  @Put("hp/:id")
  public async updateHp (@Param("id") id: number, @Body() payload: CreateHpInput) {
    return await this.tagSevice.updateHp(id, payload)
  }

}
