import * as bcrypt from 'bcrypt'
import * as jwt from 'jsonwebtoken'
import { Injectable } from '@nestjs/common';
import { User } from '../db/user.entity'
import { Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { SignupInput } from './SignupInput.dto';
import { CreateUserInput } from './CreateUserInput.dto'
import { UpdateUserInput } from './UpdateUserInput.dto'
import { UserRequest as UserRequestObject } from '../commons/user-request.dto'
import { PaginationInput } from '../commons/PaginationInput.dto';

const APP_SECRET = process.env.APP_SECRET

@Injectable()
export class UserService {
  constructor (
    @InjectRepository(User) private readonly userRepository: Repository<User>
  ) {}

  public async signUpAdmin (input: SignupInput) : Promise<User> {
    let user = this.userRepository.create({
      nama: input.nama,
      username: input.username,
      password: input.password,
      role: 'admin'
    })
    user = await this.userRepository.save(user)
    return user
  }

  /**
   * Create user.
   * @param createUserInput Payload of user data
   */
  public async createUser(createUserInput: CreateUserInput) : Promise<User> {
    const password = await bcrypt.hash(createUserInput.password, 10)
    let user = this.userRepository.create({
      ...createUserInput,

      // Use hashed password
      password
    })

    user = await this.userRepository.save(user)
    user.password = ''
    return user
  }

  /**
   * Update user data.
   * @param id Id of user
   * @param updateUserInput Attributes to update
   */
  public async updateUser(id: number, updateUserInput: UpdateUserInput) : Promise<User> {
    let user = await this.userRepository.findOneOrFail(id)
    const password = await bcrypt.hash(updateUserInput.password, 10)
    user.nama = updateUserInput.nama
    user.username = updateUserInput.username

    // Use hashed password
    user.password = password

    await this.userRepository.save(user)
    return user
  }

  public async findUserByUsernameAndPassword(username: string, password: string) : Promise<User> {
    const user = await this.userRepository.findOneOrFail({ username })
    const passswordMatch = await bcrypt.compare(password, user.password)
    if (!passswordMatch) {
      throw new Error("Password not match")
    }
    return user
  }


  /**
   * Generate token from user.
   * @param user User to generate token from
   */
  public async generateToken(user: User) : Promise<string> {
    const payload: UserRequestObject = {
      id: user.id,
      nama: user.nama,
      username: user.username,
      role: user.role
    }
    return (await jwt.sign(payload, APP_SECRET)) as string
  }

  public async decodeToken(token: string) : Promise<UserRequestObject> {
    let result = await jwt.verify(token, APP_SECRET)
    return result as UserRequestObject
  }

  public async listOrdinaryUsers (pagination: PaginationInput) : Promise<User[]> {
    let result = await this.userRepository.find({
      where: {
        role: 'user'
      },
      skip: pagination.skip,
      take: pagination.take
    })

    return result.map(user => {
      user.password = ''
      return user
    })
  }
  
  public async findUserById (id: number) : Promise<User> {
    let user = await this.userRepository.findOneOrFail(id)
    user.password = ''
    return user
  }
}
