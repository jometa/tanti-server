import { 
  Controller, 
  Post, Body, 
  Request,
  SetMetadata,
  Get,
  Query, Param
} from '@nestjs/common';
import { UserService } from './user.service';
import { SignupInput } from './SignupInput.dto';
import { LoginResult } from './LoginResult.dto'
import { PaginationInput } from '../commons/PaginationInput.dto';

@Controller('v1/user')
export class UserController {

  constructor (
    private readonly userService: UserService
  ) {}
  
  @Post('signup-admin')
  async signupAdmin (@Body() payload: SignupInput) : Promise<string> {
    const admin = await this.userService.createUser({
      nama: payload.nama,
      username: payload.username,
      password: payload.password,
      role: 'admin'
    })
    return 'OK'
  }

  @Post('login-admin')
  async loginAdmin (
    @Body('username') username: string, 
    @Body('password') password : string
  ) : Promise<LoginResult> {
    console.log('username=', username)
    console.log('password=', password)
    let user = await this.userService.findUserByUsernameAndPassword(username, password)
    
    // Do not return password to client!
    user.password = ""
    
    const token = await this.userService.generateToken(user)
    return {
      user,
      token
    }
  }

  @Get('ordinary')
  async listUsers (@Query() pagination: PaginationInput) {
    return this.userService.listOrdinaryUsers(pagination)
  }

  @Get('ordinary/:id')
  async findUserById (@Param() id: number) {
    return this.userService.findUserById(id)
  }

  @Post('signup')
  async signupUser (@Body() payload: SignupInput) {
    return await this.userService.createUser({
      nama: payload.nama,
      username: payload.username,
      password: payload.password,
      role: 'user'
    })
  }
}
