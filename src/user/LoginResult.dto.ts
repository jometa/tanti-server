import { User } from '../db/user.entity'

export class LoginResult {
  user: User
  token: string
}