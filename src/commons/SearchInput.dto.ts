import { ApiModelProperty } from "@nestjs/swagger";

export class SearchInput {
  @ApiModelProperty({ required: false })
  keyword?: string

  @ApiModelProperty()
  skip: number

  @ApiModelProperty()
  take: number
}