export function parseSolusi(data: any) {
  try {
    const result = JSON.parse(data);
    return result;
  } catch (err) {
    return data;
  }
}
