import { ApiModelProperty } from "@nestjs/swagger";

export class UserRequest {
  @ApiModelProperty({
    description: 'User id'
  })
  id: number

  @ApiModelProperty({
    description: 'User full name'
  })
  nama: string

  @ApiModelProperty({
    description: 'username'
  })
  username: string

  @ApiModelProperty({
    description: 'Role of user'
  })
  role: string
}