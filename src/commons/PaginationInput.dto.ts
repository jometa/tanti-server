import { ApiModelProperty } from "@nestjs/swagger";

export class PaginationInput {
  @ApiModelProperty()
  skip: number

  @ApiModelProperty()
  take: number
}