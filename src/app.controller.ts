import { 
  Controller, 
  Session, 
  UseInterceptors, 
  Body, 
  Header, 
  Get, 
  Param,
  Post, 
  Render,
  Res,
  UseGuards, 
  Req 
} from '@nestjs/common'
import { FileFieldsInterceptor } from '@nestjs/platform-express'
import { Response } from 'express'

import { AuthGuard } from './auth.guard'
import { AppService } from './app.service'
import { SignupInput } from './user/SignupInput.dto'
import { LoginInput } from './user/LoginInput.dto'
import { UserService } from './user/user.service'
import { CasebaseService } from './casebase/casebase.service'
import { Roles } from './commons/roles.decorator'
import { parseSolusi } from './commons/parseSolusi'

@Controller()
export class AppController {

  constructor(
    private readonly appService: AppService,
    private readonly userService: UserService,
    private readonly casebaseService: CasebaseService,
  ) {}

  @Get('')
  @Render('index')
  getHello() {
    return { msg: 'Hello' };
  }

  @Get('login')
  @Render('login')
  getLogin () {
    return { msg: 'Good' }
  }

  @Post('login')
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'username' },
    { name: 'password' }
  ]))
  @Render('redirect')
  async postLogin (
    @Body() payload: LoginInput,
    @Session() session: any
  ) {
    console.log('payload=', payload)
    const user = await this.userService.findUserByUsernameAndPassword(
      payload.username,
      payload.password
    )
    const token = await this.userService.generateToken(user)
    session.token = token
    session.save()

    // Return redirect location
    return {
      location: '/app/listing'
    }
  }

  @Get('logout')
  @Render('redirect')
  async logout (@Session() session: any) {
    await session.destroy()
    return {
      location: '/login'
    }
  }

  @Get('signup')
  @Render('signup')
  getSignup () {
    return { x : 'y' }
  }

  @Post('signup')
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'nama' },
    { name: 'username' },
    { name: 'password' }
  ]))
  @Render('redirect')
  async postSignup (@Body() payload: SignupInput) {
    // console.log('payload')
    // console.log(payload)
    // console.log()
    const result = await this.userService.createUser({
      ...payload,
      role: 'user'
    })
    return { result, location: '/login' }
  }

  @Get('app/listing')
  @Roles('user')
  @UseGuards(AuthGuard)
  @Render('listing')
  async getListing (@Req() req: any) {
    const userId = req.user.id as number
    const records = await this.casebaseService.findRecordsForUser(userId, { skip: 0, take: 1000 })
    return { active: 'listing', records }
  }

  @Get('app/record/:id')
  @Roles('user')
  @UseGuards(AuthGuard)
  @Render('record')
  async getRecord (@Param('id') id : number) {
    const record = await this.casebaseService.findRecordById(id)
    const solusi = parseSolusi(record.solusi)
    return { active: 'listing', record, solusi }
  }

  @Get('app/delete-record/:id')
  @Roles('user')
  @UseGuards(AuthGuard)
  @Render('redirect')
  async deleteRecord(@Param('id') id: number) {
    await this.casebaseService.removeRecordById(id);
    return {
      location: '/app/listing'
    };
  }

  @Get('app/diagnosa')
  @Roles('user')
  @UseGuards(AuthGuard)
  @Render('form-diagnosa')
  getFormDiagnosa () {
    return {
      active: 'diagnosa',
      listGejala: [
        { id: 1, nama: 'Gejala 1' },
        { id: 2, nama: 'Gejala 2' },
        { id: 3, nama: 'Gejala 3' }
      ]
    }
  }

    
}
