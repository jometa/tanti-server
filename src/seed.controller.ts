import { 
  Controller, 
  Session, 
  UseInterceptors, 
  Body, 
  Header, 
  Get, 
  Param,
  Post, 
  Render,
  Res,
  UseGuards, 
  Req 
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm';
import { User } from './db/user.entity';
import { Gejala } from './db/gejala.entity';
import { HamaPenyakit } from './db/hamapenyakit.entity';
import { Record, RecordType } from './db/record.entity';

@Controller('seed')
export class SeedController {

  constructor (
    @InjectRepository(Gejala)
    private readonly gejalaRepo: Repository<Gejala>,

    @InjectRepository(HamaPenyakit)
    private readonly hpRepo: Repository<HamaPenyakit>,

    @InjectRepository(Record)
    private readonly recordRepo: Repository<Record>,
  ) {}

  @Get()
  public async getSeed () {
    const allGejala = await this.gejalaRepo.find()
    const allGejalaIds = allGejala.map(g => g.id)
    const allHp = await this.hpRepo.find()
  }

}