import { ApiModelProperty } from "@nestjs/swagger";

export class CreateCaseInput {
  @ApiModelProperty({ required: false })
  public waktu?: Date

  @ApiModelProperty()
  public listGejalaId: number[]

  @ApiModelProperty()
  public idHamaPenyakit: number

  @ApiModelProperty()
  public solusi: string[]

  @ApiModelProperty()
  public desa: string

  @ApiModelProperty()
  public umurPadi: string
}