import { ApiModelProperty } from "@nestjs/swagger";

export class CreateRecordInput {
  @ApiModelProperty({ required: false })
  waktu?: Date

  @ApiModelProperty()
  listGejalaId: number[]

  @ApiModelProperty()
  idHamaPenyakit: number

  @ApiModelProperty()
  solusi: string[]

  @ApiModelProperty()
  similarity: number

  @ApiModelProperty()
  desa: string

  @ApiModelProperty()
  umurPadi: string
}