import { CreateRecordInput } from './CreateRecordInput.dto';

export class RecordDTO extends CreateRecordInput {
  id: number;
}