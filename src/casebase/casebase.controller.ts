import { 
  Controller, 
  Post, Get, Put, Delete,
  Body, Param, Query,
  UseGuards,
  Req
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'
import { Roles } from '../commons/roles.decorator'
import { CreateCaseInput } from '../casebase/CreateCaseInput.dto'
import { AuthGuard } from '../auth.guard';
import { CasebaseService } from './casebase.service'
import { CreateRecordInput } from './CreateRecordInput.dto';
import { PaginationInput } from '../commons/PaginationInput.dto'
import { Repository, LessThan } from 'typeorm';
import { User } from '../db/user.entity';
import { Gejala } from '../db/gejala.entity';
import { HamaPenyakit } from '../db/hamapenyakit.entity';
import { Record, RecordType } from '../db/record.entity';
import { RevisiDTO } from './Revisi.dto';

@Controller('v1/casebase')
export class CasebaseController {

  constructor (
    private readonly casebaseService: CasebaseService,

    @InjectRepository(Gejala)
    private readonly gejalaRepo: Repository<Gejala>,

    @InjectRepository(HamaPenyakit)
    private readonly hpRepo: Repository<HamaPenyakit>,

    @InjectRepository(Record)
    private readonly recordRepo: Repository<Record>,

    @InjectRepository(User)
    private readonly userRepo: Repository<User>
  ) {}
  
  @Post('case')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async createCaseForAdmin (
    @Req() req: any,
    @Body() payload: CreateCaseInput
  ) {
    const userId = req.user!.id
    const result = await this.casebaseService.createCase(userId, payload)
    return result
  }

  @Get('case')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async findAllCase () {
    return await this.casebaseService.findAllCase()
  }

  @Get('case/:id')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async findCaseById (@Param('id') id: number) {
    return this.casebaseService.findCaseById(id)    
  }
  
  @Delete('case/:id')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async removeCaseById (@Param('id') id: number) {
    await this.casebaseService.removeCaseById(id)
    return 'OK'
  }

  @Put('case/:id')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async updateCase (@Param('id') id: number, @Body() payload: CreateCaseInput) {
    await this.casebaseService.updateCase(id, payload)
    return 'OK'
  }

  @Post('record')
  @Roles('user')
  @UseGuards(AuthGuard)
  async createRecord (
    @Req() req: any,
    @Body() payload: CreateRecordInput
  ) {
    const userId = req.user.id
    return await this.casebaseService.createRecord(userId, payload)
  }

  @Get('record')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async findRecords (@Query() pagination: PaginationInput) {
    return await this.casebaseService.findRecords(pagination)
  }

  @Get('record/revise')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async findUnderThreesholdRecords (@Query() pagination: PaginationInput) {
    return await this.casebaseService.findRecordUnderThreeshold(pagination)
  }

  @Put('record/revise/:id')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async reviseNew(@Param('id') id: number, @Body() gejalaIds: number[]) {
    const reviseValidity = await this.casebaseService.checkReviseValidity(gejalaIds);
    if (reviseValidity) {
      return await this.casebaseService.reviseRecordNew(id, gejalaIds);
    } else {
      return await this.casebaseService.reviseRecordRedundant(id, gejalaIds);
    }
  }

  @Put('record/reviseRedundant/:id')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async reviseRedundant(@Param('id') id: number, @Body() gejalaIds: number[]) {
    return await this.casebaseService.reviseRecordRedundant(id, gejalaIds);
  }

  @Post('record/revisiV2')
  async revisiCbr(@Body() payload: RevisiDTO) {
    return await this.casebaseService.revisiCbr(payload);
  }

  @Get('record/user/:userId')
  @Roles('user', 'admin')
  @UseGuards(AuthGuard)
  async findRecordsByUser (
    @Param('userId') userId: number, 
    @Query() pagination: PaginationInput
  ) {
    return await this.casebaseService.findRecordsForUser(userId, pagination)
  }

  @Get('record/:id')
  @Roles('admin', 'user')
  @UseGuards(AuthGuard)
  async findRecordById (@Param('id') id: number) {
    return await this.casebaseService.findRecordById(id)
  }

  @Delete('record/:id')
  @Roles('admin', 'user')
  @UseGuards(AuthGuard)
  async removeRecordById (@Param('id') id: number) {
    await this.casebaseService.removeRecordById(id)
  }

  @Post('add-from-record/:id')
  @Roles('admin')
  @UseGuards(AuthGuard)
  async addRecordToDatabase (@Param('id') id: number) {
    await this.casebaseService.addRecordToCasebase(id)
  }

  @Get('summary/gejala')
  @Roles('admin', 'user')
  @UseGuards(AuthGuard)
  async countGejala () {
    const count = await this.gejalaRepo.count({
      deleted: false
    })
    return {
      count
    }
  }

  @Get('summary/hp')
  @Roles('admin', 'user')
  @UseGuards(AuthGuard)
  async countHp () {
    const count = await this.hpRepo.count({
      deleted: false
    })
    return {
      count
    }
  }

  @Get('summary/rm')
  @Roles('admin', 'user')
  @UseGuards(AuthGuard)
  async countRekamMedik () {
    const count = await this.recordRepo.count({ tipe: RecordType.CASE })
    return {
      count
    }
  }

  @Get('summary/user')
  @Roles('admin', 'user')
  @UseGuards(AuthGuard)
  async countUser () {
    const count = await this.userRepo.count({
      role: 'user'
    })
    return {
      count
    }
  }

  @Get("summarizeHp")
  @Roles("admin", "user")
  @UseGuards(AuthGuard)
  async summarizeHp () {
    return await this.casebaseService.summarizeHp()
  }


}
