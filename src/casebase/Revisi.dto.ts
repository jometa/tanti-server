export class RevisiDTO {
  id: number;
  idHamaPenyakit: number;
  gejalaIds: any[];
  solusi: string[];
}