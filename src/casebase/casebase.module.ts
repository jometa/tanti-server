import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import { CasebaseService } from './casebase.service'
import { CasebaseController } from './casebase.controller'
import { Gejala } from '../db/gejala.entity'
import { Record } from '../db/record.entity'
import { User } from '../db/user.entity'
import { HamaPenyakit } from '../db/hamapenyakit.entity'

@Module({
  imports: [
    TypeOrmModule.forFeature([Gejala, HamaPenyakit, Record, User])
  ],
  providers: [CasebaseService],
  controllers: [CasebaseController],
  exports: [CasebaseService]
})
export class CasebaseModule {
}
