import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm'
import { Connection, Repository, LessThan } from 'typeorm'
import { Record, RecordType } from '../db/record.entity'
import { Gejala } from '../db/gejala.entity'
import { HamaPenyakit } from '../db/hamapenyakit.entity'
import { PaginationInput } from '../commons/PaginationInput.dto'
import { CreateRecordInput } from './CreateRecordInput.dto'
import { CreateCaseInput } from './CreateCaseInput.dto'
import { RecordDTO } from './Record.dto';
import * as moment from 'moment'
import { RevisiDTO } from './Revisi.dto';

@Injectable()
export class CasebaseService {
  constructor (
    @InjectRepository(Record) 
    private readonly recordRepository: Repository<Record>,

    @InjectRepository(Gejala) 
    private readonly gejalaRepository: Repository<Gejala>,

    @InjectConnection()
    private readonly connection: Connection
  ) {}

  public async createRecord (userId: number, payload: CreateRecordInput) {
    let waktu = payload.waktu ? payload.waktu : moment().toDate()
    let record = this.recordRepository.create({
      waktu,
      idAddedBy: userId,
      tipe: RecordType.RECORD,
      similarity: payload.similarity,
      idHamaPenyakit: payload.idHamaPenyakit,
      umurPadi: payload.umurPadi,
      desa: payload.desa,
      // Serialize solusi to json
      solusi: JSON.stringify(payload.solusi)
    })

    // Find gejala
    const listGejala = await this.gejalaRepository.findByIds(payload.listGejalaId)
    record.listGejala = listGejala

    record = await this.recordRepository.save(record)
    return record
  }

  public async createCase (adminId: number, payload: CreateCaseInput) {
    let waktu = payload.waktu ? payload.waktu : moment().toDate()
    let case_ = this.recordRepository.create({
      waktu,
      idAddedBy: adminId,
      idHamaPenyakit: payload.idHamaPenyakit,
      tipe: RecordType.CASE,
      umurPadi: payload.umurPadi,
      desa: payload.desa,

      // When the admin add case KB, assume the similarity is 1.0
      similarity: 1.0,

      // Serialize solusi to json
      solusi: JSON.stringify(payload.solusi)
    })

    // Find gejala
    const listGejala = await this.gejalaRepository.findByIds(payload.listGejalaId)
    case_.listGejala = listGejala

    this.recordRepository.save(case_)

    return case_
  }

  public async findRecords (pagination: PaginationInput) {
    return await this.recordRepository.find({
      skip: pagination.skip,
      take: pagination.take,
      where: {
        tipe: RecordType.RECORD
      },
      relations: ['addedBy', 'listGejala', 'hamaPenyakit']
    })
  }

  public async findRecordUnderThreeshold(pagination: PaginationInput) {
    return await this.recordRepository.find({
      skip: pagination.skip,
      take: pagination.take,
      where: {
        tipe: RecordType.RECORD,
        similarity: LessThan(0.7)
      },
      relations: ['addedBy', 'listGejala', 'hamaPenyakit']
    })
  }

  public async findRecordsForUser (userId: number, pagination: PaginationInput) : Promise<Record[]> {
    return await this.recordRepository.find({
      skip: pagination.skip,
      take: pagination.take,
      where: {
        idAddedBy: userId
      },
      relations: ['listGejala', 'hamaPenyakit']
    })
  }

  public async findRecordById (id: number) : Promise<Record> {
    return await this.recordRepository.findOneOrFail(id, {
      relations: ['addedBy', 'listGejala', 'hamaPenyakit']
    })
  }

  public async findAllCase () : Promise<Record[]> {
    return await this.recordRepository.find({
      where: {
        tipe: RecordType.CASE
      },
      relations: ['addedBy', 'listGejala', 'hamaPenyakit']
    })
  }

  public async findCaseById (id: number) : Promise<Record> {
    return await this.recordRepository.findOneOrFail(id, {
      relations: ['addedBy', 'listGejala', 'hamaPenyakit']
    })
  }

  public async removeRecordById (id: number) {
    await this.recordRepository.delete(id)
  }

  public async removeCaseById (id: number) {
    await this.recordRepository.delete(id)
  }

  public async updateCase (id: number, payload: CreateCaseInput) {
    let case_ = await this.recordRepository.findOneOrFail(id)

    case_.idHamaPenyakit = payload.idHamaPenyakit
    
    const listGejala = await this.gejalaRepository.findByIds(payload.listGejalaId)
    case_.listGejala = listGejala

    case_.solusi = JSON.stringify(payload.solusi)
    case_.desa = payload.desa
    case_.umurPadi = payload.umurPadi
    case_.waktu = payload.waktu

    await this.recordRepository.save(case_)
  }

  public async addRecordToCasebase (id: number) {
    let record = await this.recordRepository.findOne(id)
    record.tipe = RecordType.CASE
    this.recordRepository.save(record)
    return record
  }

  public async checkReviseValidity(gejalaIds: any[]) {
    let gejalaIdsAsNumber = gejalaIds.map(i => parseInt(i));
    let result = await this.recordRepository.findAndCount({
      where: {
        tipe: RecordType.CASE
      },
      relations: ["listGejala"]
    });
    let items = result[0];
    console.log(gejalaIdsAsNumber);
    let similarCase = items.find(it => {
      return it.listGejala.every(gejala => gejalaIdsAsNumber.includes(gejala.id))
    });
    return similarCase === undefined;
  }

  public async reviseRecordNew(id: number, gejalaIds: number[]) {
    let record = await this.recordRepository.findOne(id);
    let newRecord = {
      ...record
    };
    delete newRecord.id;
    let listGejala = await this.gejalaRepository.findByIds(gejalaIds);
    newRecord.tipe = RecordType.CASE;
    newRecord.similarity = 1;

    newRecord.listGejala = listGejala;
    await this.recordRepository.save(newRecord);

    record.tipe = RecordType.RECORD;
    record.similarity = 1;
    
    await this.recordRepository.save(record);

    return newRecord;
  }

  public async reviseRecordRedundant(id: number, gejalaIds: number[]) {
    let record = await this.recordRepository.findOne(id);
    let listGejala = await this.gejalaRepository.findByIds(gejalaIds);
    record.tipe = RecordType.CASE;
    record.similarity = 1;

    record.listGejala = listGejala;
    await this.recordRepository.save(record);
    return record;
  }

  public async summarizeHp () {
    const q = `
      SELECT hp.id, hp.nama, count(rec.id) 
      FROM hama_penyakit hp 
      LEFT JOIN record rec ON rec."idHamaPenyakit" = hp."id" GROUP BY hp.id
    `
    return (await this.connection.query(q))
  }

  public async revisiCbr(payload: RevisiDTO) {

    let currentGejalaIds = payload.gejalaIds.map(gejalaIdString => parseInt(gejalaIdString));
    console.log(currentGejalaIds);

    // Check if there is case with (penyakitId)
    let casesWithPenyakit = await this.recordRepository.find({
      where: {
        idHamaPenyakit: payload.idHamaPenyakit,
        tipe: RecordType.CASE
      },
      relations: ['listGejala']
    });
    // console.log('====casesWithPnyakit===');
    // console.log(casesWithPenyakit);
    // console.log();
    
    // Check if there is cases with (gejala) in casesWithPenyakit
    let similarCase = casesWithPenyakit.find(record => {
      let listGejalaIds = record.listGejala.map(g => g.id);
      console.log('===listGejala===');
      console.log(listGejalaIds);
      console.log();
      return listGejalaIds.every(gid => {
        return currentGejalaIds.includes(gid);
      });
    });
    // console.log('====similarCase===');
    // console.log(similarCase);
    // console.log();

    let record = await this.recordRepository.findOne(payload.id);
    record.idHamaPenyakit = payload.idHamaPenyakit;
    record.solusi = JSON.stringify(payload.solusi);
    
    // Raise the similarity.
    record.similarity = 1;
    let result: any;

    // There is a case with similar attributes.
    if (similarCase !== undefined) {
      result = {
        similarId: similarCase.id,
        message: 'Terdapat kasus dengan penyakit dan gejala yang sama'
      };
    } else {
      record.tipe = RecordType.CASE;
      result = {
        message: 'Sukses menambahkan hasil revisi kedalam basis kasus'
      };
    }

    record = await this.recordRepository.save(record);
    return result;
  }
}