filepath = 'data-gejala.txt'
data = None
with open(filepath) as f:
    data = f.readlines()

values = []
for idx, line in enumerate(data):
    id = idx + 1
    value = f"('(G{id}) {line.strip()}')"
    values.append(value)

values = ", ".join(values)
insert_statement = f'INSERT INTO gejala ("nama") VALUES {values}'
print(insert_statement)