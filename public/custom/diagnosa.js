var API_HOST = 'http://localhost:5000';
var VEGETATIF = 'Vegetatif';
var GENERATIF = 'Generatif';
var REGEX_GEJALA_APP_ID = /[(]G(\d{1,5})[)]/i;

function parseAppIdInGejalaName(name) {
  var result = REGEX_GEJALA_APP_ID.exec(name);
  if (!result) {
    // If we can't find the pattern, return large value.
    return 10000;
  }
  return parseInt(result[1]);
}

var app = new Vue({
  el: '#vueApp',
  data: {
    listGejala: [],
    lahan: '',
    desa: '',
    umurPadi: VEGETATIF,
    currentGejalaIndex: -1,
    mode: 'info'
  },
  computed: {
    currentGejala () {
      const index = this.currentGejalaIndex
      if (index < 0) return undefined
      return this.listGejala[index]
    },
    remainingGejala () {
      const index = this.currentGejalaIndex + 1
      const listGejala = this.listGejala
      const nShow = 8

      if (index < 0) return []
      if (index >= (listGejala.length - 1)) return []

      if (listGejala.length == 0) return []
      let max = index + nShow
      if (max >= (listGejala.length - 1)) {
        max = listGejala.length
      }
      const remaining = listGejala.slice(index, max)

      // Add some fading class
      const withFading = remaining.map((gejala, index) => {
        return {
          ...gejala,
          classes: `diagnosa-remaining-${index + 1}`
        }
      })

      return withFading
    },
    selectedGejala () {
      return this.listGejala.filter(gejala => gejala.selected)
    }
  },
  mounted () {
    this.loadGejala()
  },
  methods: {
    loadGejala () {
      return fetch(`${API_HOST}/v1/tag/gejala`)
        .then(resp => resp.json())
        .then(listGejala => {
          var temp = listGejala.map(gejala => {
            return {
              ...gejala,
              selected: false
            }
          });
          // temp.forEach(gejala => {
          //   parseAppIdInGejalaName(gejala.nama);
          // });
          // console.log(temp);
          temp = temp.sort((g1, g2) => {
            return parseAppIdInGejalaName(g1.nama) - parseAppIdInGejalaName(g2.nama);
          });
          temp.forEach(g => {
            console.log(parseAppIdInGejalaName(g.nama));
          });
          // temp.reverse();
          // console.log(temp);
          this.listGejala = temp;
          this.currentGejalaIndex = 0;
        })
        .catch(err => {
          console.log(err)
          alert('Gagal memuat data gejala')
        })
    },
    diagnosa () {
      return fetch(`${API_HOST}/v1/retrieve`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          desa: this.desa,
          umurPadi: this.umurPadi,
          listGejala: this.selectedGejala.map(({ id }) => id)
        })
      })
        .then(resp => {
          if (resp.status != 201) {
            alert('Gagal mendiagnosa penyakit anda')
          }
          return resp.json()
        })
        .then(data => {
          console.log('data=', data)
          const { id } = data
          window.location = `/app/record/${id}`
        })
        .catch(err => {
          console.log(err)
          alert('Gagal')
        })
      },
    next (val) {
      const listGejala = this.listGejala
      const index = this.currentGejalaIndex
      
      if (listGejala.length == 0) return
      if (index < 0) return
      if (index >= (listGejala.length - 1)) {
        listGejala[index].selected = val
        this.mode = 'review'
        return
      }

      listGejala[index].selected = val

      this.currentGejalaIndex = index + 1
    },
    prev () {
      if (this.currentGejalaIndex === 0) return;
      this.currentGejalaIndex -= 1
    },

    reset () {
      this.selectedGejala = []
      this.currentGejalaIndex = 0
      this.mode = 'asking'
      this.listGejala.forEach(gejala => {
        gejala.selected = false
      })
    }
  }
})